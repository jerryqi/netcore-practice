﻿using System;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MangoDB
{
    class Program
    {
        static void Main(string[] args)
        {
            //建立连接
            var client = new MongoClient("mongodb://127.0.0.1:37017");
            //建立数据库
            var database = client.GetDatabase("foo");
            //建立collection
            var collection = database.GetCollection<BsonDocument>("bar");

            //为方便测试, 清除部分历史记录
            var df = Builders<BsonDocument>.Filter.Gte("counter", 0);
            collection.DeleteMany(df);

            //插入单条记录
            var document = new BsonDocument
            {
                { "name", "MongoDB" },
                { "type", "Database" },
                { "count", 1 },
                { "info", new BsonDocument
                    {
                        { "x", 203 },
                        { "y", 102 }
                    }}
            };
            collection.InsertOne(document);
            //异步插入
            //await collection.InsertOneAsync(document);
            //插入多条记录
            // generate 100 documents with a counter ranging from 0 - 99
            var documents = Enumerable.Range(0, 100).Select(i => new BsonDocument("counter", i));
            collection.InsertMany(documents);
            //异步插入
            //await collection.InsertManyAsync(documents);

            //计数
            var count = collection.Count(new BsonDocument());
            Console.WriteLine("Count:{0}", count);

            //异步计数
            //var count = await collection.CountAsync(new BsonDocument());
            //查询
            //首条记录
            var query1 = collection.Find(new BsonDocument()).FirstOrDefault();
            Console.WriteLine("Query1:{0}", query1);
            //条件查询
            var filter = Builders<BsonDocument>.Filter.Eq("counter", 71);
            var query2 = collection.Find(filter).FirstOrDefault();
            Console.WriteLine("Query2:{0}", query2);
            //全部记录
            var query3 = collection.Find(new BsonDocument()).ToList();
            Console.WriteLine("Query3:{0}", query3.ToJson());

            Console.WriteLine("Hello World!");
        }
    }
}
