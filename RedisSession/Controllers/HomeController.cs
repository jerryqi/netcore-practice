﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RedisSession.Models;

namespace RedisSession.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index(string sn)
        {
            if (!string.IsNullOrEmpty(sn))
                HttpContext.Session.Set("sn", System.Text.Encoding.Default.GetBytes(sn));
            return View();
        }

        public IActionResult Privacy()
        {
            byte[] snb = System.Text.Encoding.Default.GetBytes("Not Set");
            if (HttpContext.Session.TryGetValue("sn", out snb))
                ViewData["sn"] = System.Text.Encoding.Default.GetString(snb);
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
