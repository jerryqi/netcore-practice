
## Init Project
```Bash
dotnet new mvc -n RedisSession
dotnet add ./RedisSession/RedisSession.csproj package Microsoft.Extensions.Caching.Redis
dotnet sln add ./RedisSession/RedisSession.csproj
```

## Prepare Env
Create a redis instance with docker:
```Bash
docker run -d -p 6379:6379 --name redisapp redis
```

## Use Redis as Session
```C#
// Add Redis
services.AddDistributedRedisCache(options =>
{
    options.Configuration = "localhost";

});
// Add Session
services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(10); // Session alive time
    options.Cookie.HttpOnly = true;// set as httponly
});
...
// Use Session
app.UseSession();
```

## Code it
Set and Get Session:
```C#
public IActionResult Index(string sn)
{
    if (!string.IsNullOrEmpty(sn))
        HttpContext.Session.Set("sn", System.Text.Encoding.Default.GetBytes(sn));
    return View();
}
public IActionResult Privacy()
{
    byte[] snb = System.Text.Encoding.Default.GetBytes("Not Set");
    if (HttpContext.Session.TryGetValue("sn", out snb))
        ViewData["sn"] = System.Text.Encoding.Default.GetString(snb);
    return View();
}
```

Show Value:
```Html
<h2>Session Name: @ViewData["Title"]</h2>
```

## Run Project
```Bash
dotnet run --project ./RedisSession/RedisSession.csproj
```

Access https://localhost:5001?sn=asd first, then access https://localhost:5001/home/privacy, you will get the session value

Referrence:
- https://www.cnblogs.com/stulzq/p/7729105.html
- https://www.cnblogs.com/jerryqi/p/11793788.html
- [Redis InMemory Cache in ASP.net MVC Core](https://garywoodfine.com/redis-inmemory-cache-asp-net-mvc-core/)