﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    ChatHub
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  10/28/2019 2:17 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  10/28/2019 2:17 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace SignalR.Hubs
{
    public class ChatHub : Hub
    {
        public async Task NewMessage(long username, string message)
        {
            await Clients.All.SendAsync("messageReceived", username, message);
        }
    }
}
