﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace Selenium
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            using (IWebDriver driver = new ChromeDriver())
            {
                driver.Navigate().GoToUrl("http://www.baidu.com");
                Thread.Sleep(1000);
                driver.FindElement(By.Id("kw")).Clear();
                Thread.Sleep(1000);
                driver.FindElement(By.Id("kw")).SendKeys("selenium");
                Thread.Sleep(1000);
                driver.FindElement(By.Id("su")).Click();
                Thread.Sleep(1000);
                driver.FindElement(By.Id("kw")).Clear();
                Thread.Sleep(1000);
                driver.FindElement(By.Id("kw")).SendKeys("齐建伟的博客");
                Thread.Sleep(1000);
                driver.FindElement(By.Id("su")).Click();
                Console.ReadKey();
            }
        }

        void FindElementsApi(IWebDriver driver)
        {
            //by id
            var byID = driver.FindElement(By.Id("cards"));

            //by class name
            var byClassName = driver.FindElements(By.ClassName("menu"));

            //by tag name 
            var byTagName = driver.FindElement(By.TagName("iframe"));

            // by name
            var byName = driver.FindElement(By.Name("__VIEWSTATE"));

            //by linked text  
            //<a href="http://www.google.com">linkedtext</a>>  
            //var byLinkText = driver.FindElement(By.LinkText("linkedtext"));

            //by partial link text
            //<a href="http://www.google.com">linkedtext</a>>
            //var byPartialLinkText = driver.FindElement(By.PartialLinkText("text"));

            //by css
            var byCss = driver.FindElement(By.CssSelector("#header .content .logo"));

            //by xpath
            var byXPath = driver.FindElements(By.XPath("//div"));

            //execute javascript
            //var jsReturnValue = (IWebElement)((IJavaScriptExecutor)driver).ExecuteScript("jsfunname");

            //get element value and attribute value
            var byIDText = byID.Text;
            var byIDAttributeText = byID.GetAttribute("id");

            //click
            driver.FindElement(By.Id("copyright")).Click();

            //Navigation
            driver.Navigate().Forward();
            driver.Navigate().Back();

            //Drag And Drop
            var element = driver.FindElement(By.Name("source"));
            IWebElement target = driver.FindElement(By.Name("target"));
            (new Actions(driver)).DragAndDrop(element, target).Perform();
        }
    }
}
