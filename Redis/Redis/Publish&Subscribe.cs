﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    Publish&Subscribe
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  11/4/2019 5:40 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  11/4/2019 5:40 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using StackExchange.Redis;

namespace Redis
{
    public class Publish_Subscribe
    {
        public Publish_Subscribe(bool isPublisher)
        {
            //创建连接
            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost:6379"))
            {
                ISubscriber sub = redis.GetSubscriber();
                if (isPublisher) Publisher(sub);
                else Subscriber(sub);
            }
        }

        private void Publisher(ISubscriber sub)
        {
            Console.WriteLine("Please Enter Message or ‘q’ to Exit");
            string input;
            do
            {
                input = Console.ReadLine();
                sub.Publish("messages", input);
            } while (input != "q");
        }

        private void Subscriber(ISubscriber sub)
        {
            //订阅名为 messages 的通道
            sub.Subscribe("messages", (channel, message) =>
            {
                //输出收到的消息
                Console.WriteLine($"[{DateTime.Now:HH:mm:ss}] {message}");
            });
            Console.WriteLine("Already Subscribe ‘messages’");
            Console.ReadKey();
        }
    }
}
