
## Init Project
```Bash
dotnet new mvc -n CookieAuth
dotnet add ./CookieAuth/CookieAuth.csproj package Microsoft.AspNetCore.Authentication.Cookies
```
Restore it if neccesarry:
```Bash
dotnet restore ./CookieAuth/CookieAuth.csproj
```

In order to use intellisence, we need to create a solution and link this project to it:
```Bash
dotnet new sln
dotnet sln add ./CookieAuth/CookieAuth.csproj
```
Referrence: https://docs.microsoft.com/zh-cn/dotnet/core/tools/dotnet-sln

## Code it
Use cookie in startup:
```C#
services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
.AddCookie(options =>
{
    options.Cookie.HttpOnly = true;
    options.Cookie.Name = "AuthCookie";
    // options.Cookie.Domain = "wse.com.cn";
    options.Cookie.Path = "/";
});
//Add Authorization
services.AddAuthorization(option =>
{
    option.AddPolicy("Permission", policy => policy.Requirements.Add(new PolicyRequirement()));
});
//Inject PolicyHandler
services.AddSingleton<IAuthorizationHandler, PolicyHandler>();

//Inject HttpContext Accessor
services.AddHttpContextAccessor();

...
app.UseAuthentication();
app.UseAuthorization();
```

## Run Project
```Bash
dotnet run --project ./CookieAuth/CookieAuth.csproj
```

## Test Project
We use postman to test the api:


Refference:
- https://docs.microsoft.com/en-us/aspnet/core/security/authentication/cookie?tabs=aspnetcore2x&view=aspnetcore-3.0
- https://github.com/aspnet/AspNetCore.Docs/tree/master/aspnetcore/security/authentication/cookie/samples
- https://blog.csdn.net/qq_33341938/article/details/90914199