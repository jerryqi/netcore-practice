﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    Const
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  11/5/2019 2:46 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  11/5/2019 2:46 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
namespace JWT.AuthCenter
{
    public static class Const
    {
        /// <summary>
        /// 这里为了演示，写死一个密钥。实际生产环境可以从配置文件读取
        /// </summary>
        public const string SecurityKey = "HLfsdjfsalHJalfjJIJasldjflJDdagjdiiJlsfgHLfsdjfsalHJalfjJIJasldjflJDdagjdiiJlsfg";
        /// <summary>
        /// 站点地址
        /// </summary>
        public const string Domain = "https://localhost:5001";

        /// <summary>
        /// 受理人，之所以弄成可变的是为了用接口动态更改这个值以模拟强制Token失效
        /// 真实业务场景可以在数据库或者redis存一个和用户id相关的值，生成token和验证token的时候获取到持久化的值去校验
        /// 如果重新登陆，则刷新这个值
        /// </summary>
        public static string ValidAudience;
    }
}
