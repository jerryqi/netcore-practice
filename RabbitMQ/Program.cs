﻿using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Demos;

namespace RabbitMQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //创建连接工厂
            ConnectionFactory factory = new ConnectionFactory
            {
                UserName = "guest",//用户名
                Password = "guest",//密码
                HostName = "localhost"//rabbitmq ip
            };
            using (IConnection conn = factory.CreateConnection())
            {
                Console.WriteLine("\nRabbitMQ Connection Succeed!");
                //创建通道
                using (IModel channel = conn.CreateModel())
                {
                    // TestExchangeManage(args, channel);
                    TestExchangeType(args, channel);
                }
            }
            //Console.WriteLine("Hello World!");
        }

        static void TestExchangeManage(string[] args, IModel channel)
        {
            // channel.ExchangeDeclare("thirdExchange", ExchangeType.Topic, false, false, null);
            channel.ExchangeDelete("thirdExchange");
        }

        static void TestExchangeType(string[] args, IModel channel)
        {
            IDemo demo = new Demo10();
            if (args.Length > 0 && args[0] == "p") demo.Producter(channel);
            else demo.Consumer(channel);
        }


    }
}
