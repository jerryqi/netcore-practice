/********************************************************************************
 * Copyright (C) 2019 WSE All Rights Reserved.
 * Organization: Wall Steet English
 * Version:      $V1.0.0.0
 * Create By:    Jerry Qi
 * Email:        jerry.qi@wallstreetenglish.com
 
 * Description:
 *
 
 ********************************************************************************/
using System;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Demos
{
    public class Demo8 : IDemo
    {
        public void Producter(IModel channel)
        {
            //声明一个队列
            channel.QueueDeclare("firstQueue", false, false, true, null);

            Console.WriteLine("\nPlease Enter Message or q to exit");

            string input;
            do
            {
                input = Console.ReadLine();
                var sendBytes = Encoding.UTF8.GetBytes(input);
                //发布消息
                channel.BasicPublish("", "firstQueue", null, sendBytes);

            } while (input.Trim().ToLower() != "q");
        }

        public void Consumer(IModel channel)
        {
            //事件基本消费者
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            //接收到消息事件
            consumer.Received += async (ch, ea) =>
            {
                var message = Encoding.UTF8.GetString(ea.Body);
                Console.WriteLine($"Got Message： {message}");
                //确认该消息已被消费
                channel.BasicAck(ea.DeliveryTag, false);
                channel.BasicCancel(ea.ConsumerTag);
                await Task.Delay(8000);
                Console.WriteLine($"ReConnect");
                channel.BasicConsume("firstQueue", false, consumer);
            };
            //启动消费者 设置为手动应答消息
            channel.BasicConsume("firstQueue", false, consumer);
            Console.WriteLine("Consumer Started");
            Console.ReadKey();
        }
    }
}