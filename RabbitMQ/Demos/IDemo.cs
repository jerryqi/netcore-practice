/********************************************************************************
 * Copyright (C) 2019 WSE All Rights Reserved.
 * Organization: Wall Steet English
 * Version:      $V1.0.0.0
 * Create By:    Jerry Qi
 * Email:        jerry.qi@wallstreetenglish.com
 
 * Description:
 *
 
 ********************************************************************************/

using RabbitMQ.Client;

namespace RabbitMQ.Demos
{
    public interface IDemo
    {
        void Producter(IModel channel);

        void Consumer(IModel channel);
    }
}