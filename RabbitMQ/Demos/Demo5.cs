/********************************************************************************
 * Copyright (C) 2019 WSE All Rights Reserved.
 * Organization: Wall Steet English
 * Version:      $V1.0.0.0
 * Create By:    Jerry Qi
 * Email:        jerry.qi@wallstreetenglish.com
 
 * Description:
 *
 
 ********************************************************************************/
using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Demos
{
    public class Demo5 : IDemo
    {
        public void Consumer(IModel channel)
        {
            //事件基本消费者
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

            //接收到消息事件
            consumer.Received += (ch, ea) =>
            {
                channel.BasicCancel(ea.ConsumerTag);
                var message = Encoding.UTF8.GetString(ea.Body);

                Console.WriteLine($"Queue:deme5收到消息： {message}");
                //确认该消息已被消费
                channel.BasicAck(ea.DeliveryTag, false);
                
                Console.WriteLine($"Comsumer:demo5，消费者已Disconnect");
            };
            //启动消费者 设置为手动应答消息
            channel.BasicConsume("demo5", false, consumer);
            Console.WriteLine($"Comsumer:demo5，消费者已启动");
            Console.ReadKey();
            channel.BasicConsume("demo5", false, consumer);
            Console.WriteLine($"Comsumer:demo5，消费者已ReConnect");
            Console.ReadKey();
        }

        public void Producter(IModel channel)
        {
            channel.ExchangeDeclare("demo5", ExchangeType.Fanout, false, false, null);
            channel.QueueDeclare("demo5", false, false, false, null);
            channel.QueueBind("demo5", "demo5", "demo5", null);

            Console.WriteLine("\nPlease Enter Message or q to exit");

            string input;
            do
            {
                input = Console.ReadLine();
                var sendBytes = Encoding.UTF8.GetBytes(input);
                //发布消息
                channel.BasicPublish("demo5", "demo5", null, sendBytes);

            } while (input.Trim().ToLower() != "q");
        }
    }
}