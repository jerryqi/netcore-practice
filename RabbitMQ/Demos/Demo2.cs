/********************************************************************************
 * Copyright (C) 2019 WSE All Rights Reserved.
 * Organization: Wall Steet English
 * Version:      $V1.0.0.0
 * Create By:    Jerry Qi
 * Email:        jerry.qi@wallstreetenglish.com
 
 * Description:
 *
 
 ********************************************************************************/
using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Demos
{
    public class Demo2 : IDemo
    {
        public void Consumer(IModel channel)
        {
            //事件基本消费者
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            //接收到消息事件
            consumer.Received += (ch, ea) =>
            {
                var message = Encoding.UTF8.GetString(ea.Body);
                Console.WriteLine($"Got Message： {message}");
                //确认该消息已被消费
                channel.BasicAck(ea.DeliveryTag, false);
            };
            //启动消费者 设置为手动应答消息
            channel.BasicConsume("secondQueue", false, consumer);
            Console.WriteLine("Consumer Started");
            Console.ReadKey();
        }

        public void Producter(IModel channel)
        {
            //定义一个Direct类型交换机
            channel.ExchangeDeclare("firstExchange", ExchangeType.Direct, false, false, null);
            //声明一个队列
            channel.QueueDeclare("secondQueue", false, false, false, null);
            //将队列绑定到交换机
            channel.QueueBind("secondQueue", "firstExchange", "firstRouteKey", null);

            Console.WriteLine("\nPlease Enter Message or q to exit");

            string input;
            do
            {
                input = Console.ReadLine();
                var sendBytes = Encoding.UTF8.GetBytes(input);
                //发布消息
                channel.BasicPublish("firstExchange", "firstRouteKey", null, sendBytes);

            } while (input.Trim().ToLower() != "q");
        }
    }
}
