/********************************************************************************
 * Copyright (C) 2019 WSE All Rights Reserved.
 * Organization: Wall Steet English
 * Version:      $V1.0.0.0
 * Create By:    Jerry Qi
 * Email:        jerry.qi@wallstreetenglish.com
 
 * Description:
 *
 
 ********************************************************************************/
using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Demos
{
    public class Demo3 : IDemo
    {
        private const int ConNum = 3;
        public void Consumer(IModel channel)
        {
            for (int i = 0; i < ConNum; i++)
            {
                ConsumerGenerator(channel, "thirdQueue" + i);
            }
            Console.ReadKey();
        }

        public void Producter(IModel channel)
        {
            channel.ExchangeDeclare("secondExchange", ExchangeType.Fanout, false, false, null);
            for (int i = 0; i < ConNum; i++)
            {
                channel.QueueDeclare("thirdQueue" + i, false, false, false, null);
                channel.QueueBind("thirdQueue" + i, "secondExchange", "secondRouteKey", null);
            }

            Console.WriteLine("\nPlease Enter Message or q to exit");

            string input;
            do
            {
                input = Console.ReadLine();
                var sendBytes = Encoding.UTF8.GetBytes(input);
                //发布消息
                channel.BasicPublish("secondExchange", "secondRouteKey", null, sendBytes);

            } while (input.Trim().ToLower() != "q");
        }

        private void ConsumerGenerator(IModel channel, string queueName)
        {
            //事件基本消费者
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

            //接收到消息事件
            consumer.Received += (ch, ea) =>
            {
                var message = Encoding.UTF8.GetString(ea.Body);

                Console.WriteLine($"Queue:{queueName}收到消息： {message}");
                //确认该消息已被消费
                channel.BasicAck(ea.DeliveryTag, false);
            };
            //启动消费者 设置为手动应答消息
            channel.BasicConsume(queueName, false, consumer);
            Console.WriteLine($"Queue:{queueName}，消费者已启动");
        }
    }
}