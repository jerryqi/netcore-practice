﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AliPay.Models;
using StackExchange.Redis;

namespace AliPay.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConnectionMultiplexer _connectionMultiplexer;

        public HomeController(ILogger<HomeController> logger, IConnectionMultiplexer multiplexer)
        {
            _logger = logger;
            _connectionMultiplexer = multiplexer;
        }

        public IActionResult Index()
        {
            var db = _connectionMultiplexer.GetDatabase();

            var num = db.StringIncrement("count");

            ViewData["num"] = num;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
