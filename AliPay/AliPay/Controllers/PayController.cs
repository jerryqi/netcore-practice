﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alipay.AopSdk.AspnetCore;
using Alipay.AopSdk.Core.Domain;
using Alipay.AopSdk.Core.Request;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AliPay.Controllers
{
    public class PayController : Controller
    {
        private readonly AlipayService _alipayService;

        public PayController(AlipayService alipayService)
        {
            _alipayService = alipayService;
        }

        #region 发起支付
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// 发起支付请求
		/// </summary>
		/// <param name="tradeno">外部订单号，商户网站订单系统中唯一的订单号</param>
		/// <param name="subject">订单名称</param>
		/// <param name="totalAmout">付款金额</param>
		/// <param name="itemBody">商品描述</param>
		/// <returns></returns>
		[HttpPost]
        public void PayRequest(string tradeno, string subject, string totalAmout, string itemBody)
        {
            // 组装业务参数model
            AlipayTradePagePayModel model = new AlipayTradePagePayModel
            {
                Body = itemBody,
                Subject = subject,
                TotalAmount = totalAmout,
                OutTradeNo = tradeno,
                ProductCode = "FAST_INSTANT_TRADE_PAY"
            };

            AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
            // 设置同步回调地址
            request.SetReturnUrl($"http://{Request.Host}/Pay/Callback");
            // 设置异步通知接收地址
            request.SetNotifyUrl("");
            // 将业务model载入到request
            request.SetBizModel(model);

            var response = _alipayService.SdkExecute(request);
            Console.WriteLine($"订单支付发起成功，订单号：{tradeno}");
            //跳转支付宝支付
            Response.Redirect(_alipayService.Options.Gatewayurl + "?" + response.Body);
        }
        #endregion

        /// <summary>
		/// 支付同步回调
		/// </summary>
		[HttpGet]
        public IActionResult Callback()
        {
            /* 实际验证过程建议商户添加以下校验。
			1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
			2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
			3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
			4、验证app_id是否为该商户本身。
			*/
            Dictionary<string, string> sArray = GetRequestGet();
            if (sArray.Count != 0)
            {
                bool flag = _alipayService.RSACheckV1(sArray);
                if (flag)
                {
                    Console.WriteLine($"同步验证通过，订单号：{sArray["out_trade_no"]}");
                    ViewData["PayResult"] = "同步验证通过";
                }
                else
                {
                    Console.WriteLine($"同步验证失败，订单号：{sArray["out_trade_no"]}");
                    ViewData["PayResult"] = "同步验证失败";
                }
            }
            return View();
        }



        #region 订单查询

        [HttpGet]
        public IActionResult Query()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Query(string tradeno, string alipayTradeNo)
        {
            /*DefaultAopClient client = new DefaultAopClient(Config.Gatewayurl, Config.AppId, Config.PrivateKey, "json", "2.0",
			    Config.SignType, Config.AlipayPublicKey, Config.CharSet, false);*/
            AlipayTradeQueryModel model = new AlipayTradeQueryModel
            {
                OutTradeNo = tradeno,
                TradeNo = alipayTradeNo
            };

            AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
            request.SetBizModel(model);

            var response = await _alipayService.ExecuteAsync(request);
            return Json(response.Body);
        }

        #endregion

        #region 订单退款

        [HttpGet]
        public IActionResult Refund()
        {
            return View();
        }

        /// <summary>
        /// 订单退款
        /// </summary>
        /// <param name="tradeno">商户订单号</param>
        /// <param name="alipayTradeNo">支付宝交易号</param>
        /// <param name="refundAmount">退款金额</param>
        /// <param name="refundReason">退款原因</param>
        /// <param name="refundNo">退款单号</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> Refund(string tradeno, string alipayTradeNo, string refundAmount, string refundReason, string refundNo)
        {
            /*DefaultAopClient client = new DefaultAopClient(Config.Gatewayurl, Config.AppId, Config.PrivateKey, "json", "2.0",
			    Config.SignType, Config.AlipayPublicKey, Config.CharSet, false);*/

            AlipayTradeRefundModel model = new AlipayTradeRefundModel();
            model.OutTradeNo = tradeno;
            model.TradeNo = alipayTradeNo;
            model.RefundAmount = refundAmount;
            model.RefundReason = refundReason;
            model.OutRequestNo = refundNo;

            AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
            request.SetBizModel(model);

            var response = await _alipayService.ExecuteAsync(request);
            return Json(response.Body);
        }

        #endregion

        #region 退款查询

        /// <summary>
        /// 退款查询
        /// </summary>
        /// <returns></returns>
        public IActionResult RefundQuery()
        {
            return View();
        }

        /// <summary>
        /// 退款查询
        /// </summary>
        /// <param name="tradeno">商户订单号</param>
        /// <param name="alipayTradeNo">支付宝交易号</param>
        /// <param name="refundNo">退款单号</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> RefundQuery(string tradeno, string alipayTradeNo, string refundNo)
        {
            /*DefaultAopClient client = new DefaultAopClient(Config.Gatewayurl, Config.AppId, Config.PrivateKey, "json", "2.0",
			    Config.SignType, Config.AlipayPublicKey, Config.CharSet, false);*/

            if (string.IsNullOrEmpty(refundNo))
            {
                refundNo = tradeno;
            }

            AlipayTradeFastpayRefundQueryModel model = new AlipayTradeFastpayRefundQueryModel();
            model.OutTradeNo = tradeno;
            model.TradeNo = alipayTradeNo;
            model.OutRequestNo = refundNo;

            AlipayTradeFastpayRefundQueryRequest request = new AlipayTradeFastpayRefundQueryRequest();
            request.SetBizModel(model);

            var response = await _alipayService.ExecuteAsync(request);
            return Json(response.Body);
        }

        #endregion

        #region 订单关闭

        public IActionResult OrderClose()
        {
            return View();
        }

        /// <summary>
        /// 关闭订单
        /// </summary>
        /// <param name="tradeno">商户订单号</param>
        /// <param name="alipayTradeNo">支付宝交易号</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> OrderClose(string tradeno, string alipayTradeNo)
        {
            /*DefaultAopClient client = new DefaultAopClient(Config.Gatewayurl, Config.AppId, Config.PrivateKey, "json", "2.0",
			    Config.SignType, Config.AlipayPublicKey, Config.CharSet, false);*/

            AlipayTradeCloseModel model = new AlipayTradeCloseModel();
            model.OutTradeNo = tradeno;
            model.TradeNo = alipayTradeNo;

            AlipayTradeCloseRequest request = new AlipayTradeCloseRequest();
            request.SetBizModel(model);

            var response = await _alipayService.ExecuteAsync(request);
            return Json(response.Body);
        }

        #endregion


        #region 私有方法
        private Dictionary<string, string> GetRequestGet()
        {
            Dictionary<string, string> sArray = new Dictionary<string, string>();

            ICollection<string> requestItem = Request.Query.Keys;
            foreach (var item in requestItem)
            {
                sArray.Add(item, Request.Query[item]);

            }
            return sArray;
        }

        private Dictionary<string, string> GetRequestPost()
        {
            Dictionary<string, string> sArray = new Dictionary<string, string>();

            ICollection<string> requestItem = Request.Form.Keys;
            foreach (var item in requestItem)
            {
                sArray.Add(item, Request.Form[item]);

            }
            return sArray;
        }
        #endregion
    }
}
