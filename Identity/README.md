Use Microsoft official demo code with svn:

1. Find the repository https://github.com/aspnet/AspNetCore.Docs/tree/master/aspnetcore/security/authentication/identity/sample/WebApp3
2. Replace '/tree/master/' with '/trunk/' in url.
3. Download with svn:
```Bash
svn checkout https://github.com/aspnet/AspNetCore.Docs/trunk/aspnetcore/security/authentication/identity/sample/WebApp3
```

Use docker to begin a mssql instance:
```Bash
docker run -d -p 1433:1433 \
              -e ACCEPT_EULA='Y' \
              -e SA_PASSWORD='Str0ngPassword!' \
              --name=mssqlapp \
              microsoft/mssql-server-linux
```
- ACCEPT_EULA: End-User Licensing Agreement
- SA_PASSWORD: Set sa password

Reference: 
- https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-configure-environment-variables
- https://blog.csdn.net/dt763C/article/details/79267771

Modify connectionstring:
```
server=.;uid=sa;pwd=Str0ngPassword!;database=IdentityDemo;
```

Update database:
```Bash
dotnet ef database update
```

Got an error:
```Bash
Could not execute because the specified command or file was not found.
Possible reasons for this include:
  * You misspelled a built-in dotnet command.
  * You intended to execute a .NET Core program, but dotnet-ef does not exist.
  * You intended to run a global tool, but a dotnet-prefixed executable with this name could not be found on the PATH.
```

Install dotnet-ef with official document:
```Bash
dotnet tool update --global dotnet-ef
```

You will got anothet error:
```Bash
error NU1202: Package dotnet-ef 3.0.1 is not compatible with netcoreapp3.0 (.NETCoreApp,Version=v3.0) / any. Package dotnet-ef 3.0.1 supports: netcoreapp2.1 (.NETCoreApp,Version=v2.1)
The tool package could not be restored.
Tool 'dotnet-ef' failed to install. This failure may have been caused by:

* You are attempting to install a preview release and did not use the --version option to specify the version.
* A package by this name was found, but it was not a .NET Core tool.
* The required NuGet feed cannot be accessed, perhaps because of an Internet connection problem.
* You mistyped the name of the tool.

For more reasons, including package naming enforcement, visit https://aka.ms/failure-installing-tool
```

In this situation, we need to install with specific version:
```Bash
dotnet tool update --global dotnet-ef --version 3.0.0-preview7.19362.6
```
Reference:
- https://docs.microsoft.com/zh-tw/ef/core/miscellaneous/cli/dotnet
- https://blog.csdn.net/topdeveloperr/article/details/101282099

Then we update database, it will be works.

Refference for Identity:
- https://www.cnblogs.com/savorboard/p/aspnetcore-identity.html
- https://www.cnblogs.com/savorboard/p/aspnetcore-identity2.html
- https://www.cnblogs.com/savorboard/p/aspnetcore-identity3.html

Create Sql Account:
```Sql
CREATE LOGIN db_user 
	WITH PASSWORD = 'L#eZ)7X[=d' 
GO
```

Run the project:
```Bash
dotnet run --project ./Identity/Identity.csproj
```