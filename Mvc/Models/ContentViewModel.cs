﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    ContentViewModel
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  10/30/2019 4:17 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  10/30/2019 4:17 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Collections.Generic;

namespace Mvc.Models
{
    public class ContentViewModel
    {
        /// <summary>
        /// 内容列表
        /// </summary>
        public List<Content> Contents { get; set; }
    }
}
