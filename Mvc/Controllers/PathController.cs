/********************************************************************************
 * Copyright (C) 2019 WSE All Rights Reserved.
 * Organization: Wall Steet English
 * Version:      $V1.0.0.0
 * Create By:    Jerry Qi
 * Email:        jerry.qi@wallstreetenglish.com
 
 * Description:
 *
 
 ********************************************************************************/
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Mvc.Controllers
{
    public class PathController : Controller
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public PathController(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            string contentRootPath = _hostingEnvironment.ContentRootPath;

            return Content(webRootPath + "\n" + contentRootPath);
        }
    }
}