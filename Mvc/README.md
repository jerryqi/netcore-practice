Add project to netcore-practice.sln
```Bash
dotnet sln add ./MVC/MVC.csproj
```

Run:
```Bash
dotnet run --project ./MVC/MVC.csproj
```

Test root path with PathController

Reference:
- https://blog.csdn.net/yhjahjj1314/article/details/81437418